node default {}

node 'client-1' {
  exec { 'apt-update':
    command => '/usr/bin/apt-get update'
} ->
package { 'apache2':
  require => Exec['apt-update'],
  ensure => installed,
} ->
class { 'apache':
  default_vhost => false,
  default_mods => false,
  mpm_module => 'prefork',
} ->
include apache::mod::php
apache::vhost { 'kaplan-devops.com':
  port    => '80',
  docroot => '/var/www/devopstest/',
} ->
file { 'version.json':
  path => '/var/www/devopstest/version.json',
  ensure => file,
  require => Class['apache'],
  source => 'https://s3-eu-west-1.amazonaws.com/kaplandevopstest/version.json',
} ~>
service { 'apache2':
  ensure => running,
  enable => true,
}
}
